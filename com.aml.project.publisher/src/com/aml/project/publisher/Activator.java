package com.aml.project.publisher;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

import java.util.Dictionary;

/**
 * This is the main publisher class for this bundle.
 * In this class, events are sent to certain topics
 *
 * @author : Akbar mohsur Alam
 * @version : 1.0.0
 * @see BundleActivator
 * @see BundleContext
 * @see Event
 */
public class Activator implements BundleActivator {

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        ServiceReference ref = bundleContext.getServiceReference(EventAdmin.class.getName());
        EventAdmin eventAdmin = (EventAdmin) bundleContext.getService(ref);

        System.out.println("event sent");

        Event toggleEvent = new Event("click/switch1", (Dictionary<String, ?>) null);
        eventAdmin.sendEvent(toggleEvent);

        Event holdEvent = new Event("hold/switch1", (Dictionary<String, ?>) null);
        eventAdmin.sendEvent(holdEvent);

    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {

    }
}
