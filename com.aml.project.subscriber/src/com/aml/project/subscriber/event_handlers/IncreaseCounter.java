package com.aml.project.subscriber.event_handlers;

import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

import java.util.Arrays;

/**
 * Event handler class to increase the counter
 * @see Event
 * @see EventHandler
 */
public class IncreaseCounter implements EventHandler {

    @Override
    public void handleEvent(Event event) {
        System.out.println( "->Increased counter<- Topic -> \t" + event.getTopic() + "\t" + Arrays.toString(event.getPropertyNames()));

    }
}
