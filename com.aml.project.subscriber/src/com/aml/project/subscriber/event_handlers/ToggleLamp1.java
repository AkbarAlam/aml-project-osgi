package com.aml.project.subscriber.event_handlers;

import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

import java.util.Arrays;

/**
 * Event handler class to toggle lamp1
 * @see Event
 * @see EventHandler
 */
public class ToggleLamp1 implements EventHandler {

    @Override
    public void handleEvent(Event event) {
        System.out.println( "->Toggle Lamp<- Topic -> \t" + event.getTopic() + "\t" + Arrays.toString(event.getPropertyNames()));

    }
}
