package com.aml.project.subscriber.event_handlers;

import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

import java.util.Arrays;

/**
 * Event handler class to toggle lamps2
 * @see Event
 */
public class ToggleLamp2 implements EventHandler {
    @Override
    public void handleEvent(Event event) {
        System.out.println( "->Toggle Lamp2<- Topic -> \t" + event.getTopic() + "\t" + Arrays.toString(event.getPropertyNames()));
    }
}
