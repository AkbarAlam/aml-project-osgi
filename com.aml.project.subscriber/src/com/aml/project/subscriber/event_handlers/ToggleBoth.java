package com.aml.project.subscriber.event_handlers;

import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

import java.util.Arrays;

/**
 * Event handler class to toggle both lamps
 * @see Event
 */
public class ToggleBoth implements EventHandler {
    @Override
    public void handleEvent(Event event) {
        System.out.println( "->Toggle Both Lamps <- Topic -> \t" + event.getTopic() + "\t" + Arrays.toString(event.getPropertyNames()));
    }
}
