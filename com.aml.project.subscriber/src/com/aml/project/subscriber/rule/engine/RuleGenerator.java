package com.aml.project.subscriber.rule.engine;

import com.aml.project.subscriber.event_handlers.IncreaseCounter;
import com.aml.project.subscriber.event_handlers.ToggleBoth;
import com.aml.project.subscriber.event_handlers.ToggleLamp1;
import com.aml.project.subscriber.event_handlers.ToggleLamp2;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.EventHandler;

import java.util.Arrays;
import java.util.Dictionary;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * This is the rule generator class.
 * Based on the defined rules by the user certain event handler will be used.
 * All event handlers have been defined initialized here.
 * In this class only public methods contains javaDoc.
 *
 * @author : Akbar mohsur Alam
 * @version : 1.0.0
 *
 * @see BundleContext
 * @see Set
 * @see Collectors
 */
public class RuleGenerator {

    private final String[] holdtopic;
    private final String[] clickTopic;


    private  ToggleLamp1 toggleLamp1 = new ToggleLamp1();
    private ToggleLamp2 toggleLamp2 = new ToggleLamp2();
    private ToggleBoth toggleBoth = new ToggleBoth();
    private IncreaseCounter increaseCounter = new IncreaseCounter();

    /***
     * Creates a Rule Generator object by taking the following parameters.
     * @param holdtopic topics for on hold actions
     * @param clickTopic topics for on click actions
     */
    public RuleGenerator(String[] holdtopic, String[] clickTopic) {
        this.holdtopic = holdtopic;
        this.clickTopic = clickTopic;
    }

    final static Set<String> predicates = Set.of("increases", "toggles");
    final static Set<String> objectsLamp = Set.of("lamp1", "lamp2","both");
    final static Set<String> objectsCounter = Set.of("counter");


    /**
     * This class generates the rules by taking the following parameters.
     * based on the conditions the handler has been assigned
     * @param text defined by user
     * @param clickProperties registered topics
     * @param onHoldProperties registered topics
     * @param context bundle context
     */
    public void generateRules(String text,Dictionary clickProperties,Dictionary onHoldProperties, BundleContext context) {
        String predicate = getPredicate(text);
        String object = getObject(text, predicate);
        String[] selector = topicSelector(text, predicate);


        if (selector == holdtopic){
            if (predicate.equals("increases") && object.equals("counter")){
                System.out.println("User Text :"+ text +"\nFor the selected topic: " + selector[0] + "\ndefined rules: The counter will be increased on the hold of switch\n\n");
                context.registerService(EventHandler.class.getName(), increaseCounter,onHoldProperties);
            }
            if (predicate.equals("toggles") && object.equals("lamp1")){
                System.out.println("User Text :"+ text +"\nFor the selected topic: " + selector[0] + "\ndefined rules: The lamp1 will be toggled on hold of switch1 \n\n");
                context.registerService(EventHandler.class.getName(), toggleLamp1,onHoldProperties);
            }
            if (predicate.equals("toggles") && object.equals("lamp2")){
                System.out.println("User Text :"+ text +"\nFor the selected topic: " + selector[0] + "\ndefined rules: The lamp2 will be toggled on hold of switch1\n\n");
                context.registerService(EventHandler.class.getName(), toggleLamp2,onHoldProperties);
            }

            if (predicate.equals("toggles") && object.equals("lamps")){
                System.out.println("User Text :"+ text +"\nFor the selected topic: " + selector[0] + "\ndefined rules: The both lamps will be toggled on hold of switch1\n\n");
                context.registerService(EventHandler.class.getName(), toggleBoth,onHoldProperties);
            }
        }

        if (selector == clickTopic){
            if (predicate.equals("increases") && object.equals("counter")){
                System.out.println("User Text :"+ text +"\nFor the selected topic: " + selector[0] + "\ndefined rules: The counter will be increased on click of switch\n\n");
                context.registerService(EventHandler.class.getName(), increaseCounter,clickProperties);
            }
            if (predicate.equals("toggles") && object.equals("lamp1")){
                System.out.println("User Text :"+ text +"\nFor the selected topic: " + selector[0] + "\ndefined rules: The lamp1 will be toggled on click of switch\n\n");
                context.registerService(EventHandler.class.getName(), toggleLamp1,clickProperties);
            }
            if (predicate.equals("toggles") && object.equals("lamp2")){
                System.out.println("User Text :"+ text +"\nFor the selected topic: " + selector[0] + "\ndefined rules: The lamp2 will be toggled on click of switch\n\n");
                context.registerService(EventHandler.class.getName(), toggleLamp2,clickProperties);
            }

            if (predicate.equals("toggles") && object.equals("both")){
                System.out.println("User Text :"+ text +"\nFor the selected topic: " + selector[0] + "\ndefined rules: both lamps will be toggled on click of switch\n\n");
                context.registerService(EventHandler.class.getName(), toggleBoth,clickProperties);
            }
        }
    }

    private String[] topicSelector(String text , String predicate){
        String[] subObj = text.split(predicate);
        String[] subjectSplit = subObj[0].split(" ");

        Set<String> clickTopicStringSet = getTopicStrings(this.clickTopic);
        Set<String> holdTopicStringsSet = getTopicStrings(this.holdtopic);

        boolean foundHold = Arrays.stream(subjectSplit).filter(e -> holdTopicStringsSet.contains(e)).findAny().toString().contains("hold");
        boolean foundClick = Arrays.stream(subjectSplit).filter(e -> clickTopicStringSet.contains(e)).findAny().toString().contains("click");

        if (foundClick){
            return this.clickTopic;
        } else if (foundHold) {
            return this.holdtopic;
        } else return new String[]{"not defined"};
    }

    private static String getPredicate(String text){
        String[] textAsArray = text.split(" ");
        List<String> collect = Arrays.stream(textAsArray).filter(predicates::contains).collect(Collectors.toList());
        return collect.get(0);
    }

    private static String getObject(String text,String predicate){
        String[] subjectObject = text.split(predicate);
        String[] objects = subjectObject[1].split(" ");

        boolean presentLamp = Arrays.stream(objects).anyMatch(objectsLamp::contains);
        boolean presentCounter = Arrays.stream(objects).anyMatch(objectsCounter::contains);

        if (presentLamp && !presentCounter) {
            List<String> collectObject = Arrays.stream(objects).filter(objectsLamp::contains).collect(Collectors.toList());
            return collectObject.get(0);
        }else if (presentCounter && !presentLamp) {
            List<String> collectObject = Arrays.stream(objects).filter(objectsCounter::contains).collect(Collectors.toList());
            return collectObject.get(0);
        } else {
            return "not defined";
        }

    }

    private static Set<String> getTopicStrings(String[] topic) {
        String[] clickTopicSplited = topic[0].split("/");
        return Arrays.stream(clickTopicSplited).collect(Collectors.toSet());
    }
}
