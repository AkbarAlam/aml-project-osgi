package com.aml.project.subscriber;

import com.aml.project.subscriber.rule.engine.RuleGenerator;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.EventConstants;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Set;
import java.util.stream.Collectors;

/**
 This is the main subscriber class for this bundle.
 * In this class, the user can define the rules. The topics for the communications are being registered
 *
 * @author : Akbar mohsur Alam
 * @version : 1.0.0
 * @see RuleGenerator
 * @see BundleActivator
 */
public class Subscriber implements BundleActivator {

    final static String [] clickTopic = new String[] {"click/switch1"};
    final static String[] onHoldTopic = new String[] {"hold/switch1"};
    final static String text = "click of switch1 toggles both lamps";

    //final static Set<String> userRules = Set.of("click of switch1 toggles lamp1", "hold of switch1 toggles lamp2");

    @Override
    public void start(BundleContext bundleContext) throws Exception {

        Set<String> userRules = Files
                .lines(Paths.get("../../rules.txt"))
                .collect(Collectors.toSet());

        RuleGenerator ruleGenerator = new RuleGenerator(onHoldTopic,clickTopic);

        Dictionary onCLickProperties = new Hashtable();
        onCLickProperties.put(EventConstants.EVENT_TOPIC, clickTopic );

        Dictionary onHoldProperties = new Hashtable();
        onHoldProperties.put(EventConstants.EVENT_TOPIC,onHoldTopic);

        userRules.stream().forEach(userDefinedText -> {
            ruleGenerator.generateRules(
                    userDefinedText,
                    onCLickProperties,
                    onHoldProperties,
                    bundleContext);
        });


    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {

    }


}


