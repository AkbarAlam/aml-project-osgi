# Event condition action machine
This repository contains the code for the ambient intelligent system project. This is the event condition action machine layer only. 

## Features.
* Communication using OSGI Event admin ( Publish subscriber paradigm) 
* The user is able to configure the rules using plain text files 

## Required dependencies ( apart from the standard dependencies ) 
* org.osgi.service.event-1.4.0.jar
* org.eclipse.osgi.services_3.80.jar
* org.eclipse.equinox.event_1.5.100.jar

This project has been done using IntelliJ IDE with the OSGi plugin. 